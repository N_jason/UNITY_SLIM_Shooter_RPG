Auteur : Jason Neves 
Titre  : Slim Shooter RPG
Sujet  : Dans un Monde en '3D' vous controler votre personnage, pour �viter les redoutables Slim

Probl�mes : 
	- Comment faire de la 3D.
		- Premi�re solution bouger juste la cam�ras. (NON)
		- Vue Isom�trique. J'ai lue quelque article sur les vues isom�trique et comment les cr�ers gr�ce a des rotations et transformation. 
		   Pour le personnage c'est un sprite que je d�place sur les axes x et z celon une animation.
		- Changement des diff�rents �l�ments vue en cours qui �tait en 2D pour ce de la 3D (Collider et sprite).
	- Comment permettre � un enemi de suivre un personnage de fa�on a peut pr�s naturel
		- Apr�s avoir chercher des mani�res de le cr�er j'ai trouver un cours vid�o que
                j'ai r�alis� dans une sc�ne a part de mon projet puis est int�gr� la solution qui convenait � mon usage.
		- Utilisation de la biblioth�que IA et le lien entre des obstacles, des enemies(NavMeshAgent) et le sol. 
	- Suivi du joueur :
		La cam�ra et la vie sont attach� au joueur.
	- Comment g�rer les sprites pour cr�er un animation sur un monde en 3D
		- La solution �tait d'utiliser l'outil animator( j'ai eu quelque mal � l'utiliser, 
		le probl�me est que l'animation en cours doit �tre terminer avant de passer � la suivante).

	- Ajouts sur mobile :
		Je n'est pas r�ussi a installer le sdk permettant d'utiliser unity sur android je n'est donc pas tester mon jeux via mobile.
		L'utilisation d'un joystick n'as donc pas �t� faite mais le traitement des mouvements permet d'�tre impl�ment� facilement.
		La lecture des direction ce fait a l'aide des diff�rents axes. et pour le tir un boutton permettrai de remplir cette t�che.
		Je n'est pas voulue passer trop de temps sur ce probl�me car je trouvait qu'il �tait plus important de r�gler les autres.

	- Des probl�mes de valeur Null �tait pr�sent 
		Ajouts de m�thode asynchrone charger apr�s les �l�ments (ex : score,GameOver...)

�volution possible :

 Pour cr�er ce jeux j'ai trouv� diff�rent sprite qui poss�des de multiple texture pour le sol et des personnages. Ce qui permettrai de rendre plus dynamique le menus.
 Diff�rents �l�ments  pourrait �tre rajouter comme bonus (Vie , tir plus rapide ,ralentissement des enemies)


Information Compl�mentaire :
	Le jeux est fait pour �tre jouer en mode portrait seulement.
	Les diff�rentes commande sont les touches directrice (d�placement )et la barre d'espace (tir) .
	Pour �viter les d�placements en dehors du terrain la velocit� du personnage devient Null et il tombe sur un collider retournant  au menu principal mais les points sont compt�es.