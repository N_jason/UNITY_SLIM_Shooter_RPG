﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;       //Allows us to use Lists. 

public class GameManager : MonoBehaviour
{

	public static GameManager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
	private static int maxPoint=0;
	private static int CurrentPoint = 0;

	//Awake is always called before any Start functions
	void Awake()
	{
		//Check if instance already exists
		if (instance == null) {

			//if not, set instance to this
			instance = this;
			GameStateManager.GameState = GameState.Intro;
		}

		//If instance already exists and it's not this:
		else if (instance != this)

			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);    

		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);

		//Get a component reference to the attached BoardManager script
		//Application.LoadLevel("level1"); 

		//Call the InitGame function to initialize the first level 
	}




	//Update is called every frame.
	void Update()
	{
	}

	public void StartPartie(){
		if (GameStateManager.GameState == GameState.Intro)
		{
			SceneManager.LoadScene("level1"); 


			GameStateManager.GameState = GameState.Playing;
			CurrentPoint = 0;

		}

	}
	public int killEnemie(){
		CurrentPoint++;
		//Debug.Log ("Score  : "+GameManager.getCurrentScore ());
		//int nbAddEnemie = Random.Range(1,3);
		//for (int i = 0; i < nbAddEnemie; i++) { // un enemi est en faite sufisant 
			GameObject genemy = Instantiate (Resources.Load ("Prefabs/enemy"), new Vector3 (Random.Range (-10, 10), 0.2f, Random.Range (-10, 10)), Quaternion.identity) as GameObject;  
			genemy.GetComponent<Enemy> ().victim = GameObject.FindGameObjectWithTag ("MainPerso").transform;
		//}
		return CurrentPoint;
	
	}
	IEnumerator loadMainMenuAsyncScene(){
		AsyncOperation aSync =  SceneManager.LoadSceneAsync ("MainMenu");

		while (!aSync.isDone) {
			yield return null;
		}
		GameObject.FindGameObjectWithTag ("CurrentPoint").GetComponent<Text>().text = getCurrentScore();
		GameObject.FindGameObjectWithTag ("MaxPoint").GetComponent<Text>().text = getMaxScore();


	}
	public void StopPartie(){
		if (GameStateManager.GameState == GameState.Playing)
		{
			StartCoroutine(loadMainMenuAsyncScene()); // Lit la scene en mode async pour permettre de mettre à jours les scores

			GameStateManager.GameState = GameState.Intro;

			if (CurrentPoint > maxPoint){
				maxPoint = CurrentPoint;

			}

		}

	
	}
	public static string getMaxScore(){
		return maxPoint.ToString();
	}
	public static string getCurrentScore(){
		return CurrentPoint.ToString();
	}

	/*bool WasTouchedOrClicked()
	{
		if (Input.GetAxis ("Vertical") !=0||Input.GetAxis ("Horizontal") != 0)
			return true;
		else
			return false;
	}*/
}