using UnityEngine;
using System.Collections;

public class shootAgain : MonoBehaviour
{
	Vector3 siz;
	private string currentcolor ="shootOrange";
	// Use this for initialization
	void Start(){
	
	}
	
	// Update is called once per frame
	void Update () { 
		// Get the size of the gameObject containing this script 
		siz.x =gameObject.GetComponent<SpriteRenderer> ().bounds.size.x; 
		siz.y=gameObject.GetComponent<SpriteRenderer> ().bounds.size.y; 
		// If space key pressed 

		if (Input.GetKeyDown (KeyCode.Space)) { 
			// Get the posi'on of the shoot using the ship posi'on 
			Vector3 tmpPosRight = new Vector3 (transform.position.x+ siz.x ,0.34f, transform.position.z); 
			Vector3 tmpPosUp = new Vector3 (transform.position.x , 0.34f, transform.position.z + siz.y); 
			Vector3 tmpPosDown = new Vector3 (transform.position.x, 0.34f, transform.position.z - siz.y); 
			Vector3 tmpPosLeft = new Vector3 (transform.position.x  - siz.x, 0.34f, transform.position.z); 
			// Instanciate 	shootOrange
			if (currentcolor == "shootOrange"){
				GameObject gR = Instantiate (Resources.Load("Prefabs/shootOrange"), tmpPosRight, Quaternion.identity) as GameObject;  
				GameObject gU = Instantiate (Resources.Load("Prefabs/shootOrange"), tmpPosUp, Quaternion.identity) as GameObject;  
				GameObject gD = Instantiate (Resources.Load("Prefabs/shootOrange"), tmpPosDown, Quaternion.identity) as GameObject;  
				GameObject gL = Instantiate (Resources.Load("Prefabs/shootOrange"), tmpPosLeft, Quaternion.identity) as GameObject;  
				gR.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;	
				gU.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX;	
				gD.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX;	
				gL.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;	
				gL.GetComponent<moveShoot> ().inverse = true;
				gD.GetComponent<moveShoot> ().inverse = true;
				currentcolor = "shootJaune";
			}else{
				//GameObject	gY = Instantiate (Resources.Load("Prefabs/shootJaune"), tmpPos, Quaternion.identity) as GameObject;  
				GameObject gR = Instantiate (Resources.Load("Prefabs/shootJaune"), tmpPosRight, Quaternion.identity) as GameObject;  
				GameObject gU = Instantiate (Resources.Load("Prefabs/shootJaune"), tmpPosUp, Quaternion.identity) as GameObject;  
				GameObject gD = Instantiate (Resources.Load("Prefabs/shootJaune"), tmpPosDown, Quaternion.identity) as GameObject;  
				GameObject gL = Instantiate (Resources.Load("Prefabs/shootJaune"), tmpPosLeft, Quaternion.identity) as GameObject;  
				gR.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;	
				gU.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX;	
				gD.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX;	
				gL.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;	
				gL.GetComponent<moveShoot> ().inverse = true;
				gD.GetComponent<moveShoot> ().inverse = true;
				currentcolor = "shootOrange";
			}


		} 
	} 

}

