﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class moveShoot: MonoBehaviour
{
	Vector3 screenTopRight;
	Vector3 screenBottomLeft;
	private Vector2 mouvement;
	private Vector3 speed;
	public bool inverse;
	void Start ()
	{
		
		speed.x = 5;
		speed.y = 0;
		speed.z = 5;
		if (inverse) {
			speed = speed * -1;
		}		

	}
	void OnTriggerEnter(Collider collider) { 
		// Add the fade script to the gameObjectcontaining this script 

		if (collider.name.Contains("enemy" ) ){
			collider.gameObject.AddComponent<fadeOut>(); 
			GameManager.instance.killEnemie ();
			// Shoot destroy 

		
		}

	} 
	void Update(){
		//mouvement = new Vector3 (0 ,0 , speed.y) ;
		if (gameObject.transform.position.x > -17 && gameObject.transform.position.x < 17 && gameObject.transform.position.z > -15 && gameObject.transform.position.z < 15)
			gameObject.GetComponent<Rigidbody> ().velocity = speed;
		
		else
			Destroy (gameObject);
	}
}