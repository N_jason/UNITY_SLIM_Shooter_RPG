using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fadeOut : MonoBehaviour
{

	void Update () { 
		Color cl = 	GetComponent<MeshRenderer>().material.color; 
		cl.g -= 0.03f; 
		//Debug.Log (cl.a);
		GetComponent<MeshRenderer>().material.color = cl; 
		if (cl.g < 0) { 
			Destroy (gameObject); 
		} 
	} 
}
