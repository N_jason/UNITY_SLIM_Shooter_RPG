﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class Enemy : MonoBehaviour {
	public Transform victim ;
	private NavMeshAgent navComponent;
	// Use this for initialization
	void Start () {
		navComponent = this.GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (victim) {
			navComponent.destination = victim.position;
		}
		
	}
	void OnTriggerEnter(Collider collider){
		//Debug.Log("colider"+collider.name);

		/**
			 * Recuperer les vie et les supprimer si il n'y en a plus le supprimer.
			 * 
			 */
		if (collider.name.Contains("perso")){
			//Debug.Log("trigger");
			if (GameObject.FindGameObjectWithTag ("life_3"))
				GameObject.FindGameObjectWithTag ("life_3").AddComponent<fadeOutSprite>();
			else if (GameObject.FindGameObjectWithTag ("life_2"))
				GameObject.FindGameObjectWithTag ("life_2").AddComponent<fadeOutSprite>();
			else if (GameObject.FindGameObjectWithTag ("life_1"))
				GameObject.FindGameObjectWithTag ("life_1").AddComponent<fadeOutSprite>();
			else {
				GameObject.FindGameObjectWithTag ("MainPerso").AddComponent<isKill>();
			}


		}
	}
}