﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class isKill : MonoBehaviour {
	IEnumerator Start() {
		

		GameObject gameOver = Instantiate (Resources.Load ("Prefabs/GameOver"), new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
		yield return StartCoroutine(WaitAndPrint(1.0F,gameOver));

	}
	IEnumerator WaitAndPrint(float waitTime,GameObject gameOver) {
		// show Game Over 
		yield return new WaitForSeconds(waitTime);
		Destroy (gameOver); 
		GameManager.instance.StopPartie ();


	}
}
