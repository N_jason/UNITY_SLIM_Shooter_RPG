﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBody : MonoBehaviour {
	private Vector3 speed;
	private float entrer_x;
	private float entrer_y;
	private bool onMove;
	private Rigidbody rbody;

	private Animator animcurrent;


	// Use this for initialization
	void Start () {
		rbody = gameObject.GetComponent<Rigidbody> ();
		animcurrent = gameObject.GetComponent<Animator> ();
		onMove = false;
		animcurrent.SetTrigger ("stop");
	}

	// Update is called once per frame
	void Update () {
		entrer_y = Input.GetAxis ("Vertical");
		entrer_x = Input.GetAxis ("Horizontal");

			
		if (entrer_y != 0 || entrer_x != 0) {
			//detection Mouvement

			if (entrer_y != 0) {//mode vertical
				entrer_x = 0;
				animcurrent.SetBool ("vert",true);
				animcurrent.SetTrigger ("Change_move");

				animcurrent.SetBool ("Hor", false);
			} else if (entrer_x != 0) {// mode horizontal 
					entrer_y = 0;
				animcurrent.SetBool ("Hor", true);
				animcurrent.SetTrigger ("Change_move");

				animcurrent.SetBool ("vert",false);
			}
			if (! onMove) {
				//Debug.Log (onMove);
				animcurrent.SetTrigger ("Change_move");
				animcurrent.SetTrigger ("stop");

			}
			getAnimation (entrer_y, entrer_x);
			onMove = true;

			if (gameObject.transform.position.x > -17 && gameObject.transform.position.x < 17 && gameObject.transform.position.z > -15 && gameObject.transform.position.z < 15)
				rbody.velocity = new Vector3 (entrer_x, 0, entrer_y) * 4;
			else
				rbody.velocity = Vector3.zero;
		} else {


			if (onMove) {
				animcurrent.SetTrigger ("Change_move");
				animcurrent.SetTrigger ("stop");
			} 
			onMove = false;

		}
			

	}
	void getAnimation(float x,float y){
		animcurrent.SetFloat ("x", x);
		animcurrent.SetFloat ("y", y);
		

	}
}

