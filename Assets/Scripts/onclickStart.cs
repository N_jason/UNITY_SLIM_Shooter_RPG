﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class onclickStart : MonoBehaviour {

	// Use this for initialization
	private Button mybtn;

	void Start () {
		mybtn = GetComponent<Button> ();
		mybtn.onClick.AddListener(()=>{GameManager.instance.StartPartie();});
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
